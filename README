polyFunctions 0.4
========================================================================

Python module providing several functions to work with polygon meshes in
Autodesk Maya.

This module requires the oppositeFace plugin to be available for some
functions, see <https://gitlab.com/goeb/oppositeface>.

You need to place the polyFunctions module in a directory where Python
can find it. Please see the Maya documentation for details on "Adding
items to your Python path". You can then import the module with

    > import polyFunctions as pf

A message in the script editor should appear stating that polyFunctions
was indeed imported. After that, you can run

    > pf.__doc__

to display a list of available functions and some general information.
To display the description of an individual function use a command like

    > pf.<function>.__doc__

or to get a complete module description - including all functions - run

    > help (pf)


Copyright And License
------------------------------------------------------------------------

Copyright 2009, 2014 Stefan Göbel - <polyfunctions —@— subtype —•— de>

This program is free software: you can redistribute it and/or modify it
under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at
your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser
General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program. If not, see <http://www.gnu.org/licenses/>.
