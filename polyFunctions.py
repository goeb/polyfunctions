# Copyright 2009, 2014 Stefan Goebel - <polyfunctions -at- subtype -dot- de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


"""
polyFunctions 0.4 - Miscellaneous functions for polygon modelling.

This module provides the following functions, please see their individual
descriptions for details:

avgVertexHeight      - Return the average Y coordinate of some vertices.
bridgeOppositeFaces  - Create a hole between opposite faces, bridge the edges.
cuboidFromPoints     - Create a polygon cuboid from a list of vertex positions.
dirVector            - Calculate the direction vector between two points.
findVertexAt         - Find vertices at a specified position.
innerProduct         - Calculate inner product of two vectors.
isMesh               - Check if objects are meshes.
joinFaces            - Delete the edges between specified faces.
parentNode           - Get the parent of an object.
parentTransform      - Get the first transform node parent of an object.
pointBetween         - Get the center between two points.
pointCoordinates     - Get the coordinates of a vertex.
pointInDir           - Find a point in a specified direction.
pointInDist          - Get a point in a specified distance from a line.
pointsDistance       - Calculate distance between two points.
polyArea2D           - Calculate signed area of a 2D polygon.
polyArea3D           - Calculate (unsigned) area of a 3D polygon.
polyInfoList         - Convert polyInfo return values to proper lists.
reduceComponents     - Remove duplicate components from a list.
removeDuplicateFaces - Delete duplicate faces of a polygon mesh.
sharedComponents     - Get polygon components shared by some other components.
unitCorrection       - Convert values in current unit to cm.
vertsAdjustVertical  - Move vertices directly above or below a reference.
"""


__version = "0.4"


import maya.cmds as cmd
import math
import re


def avgVertexHeight (verts = None, move = False, history = False):

    """Get the average Y coordinate of vertices and (optionally) move them.

    First parameter must be a list of vertex names, if no parameter is
    specified, the current selection will be used. Everything that is not a
    vertex will be silently ignored. The function will return the average Y
    coordinate or None in case of an error. If the named parameter move is set
    to True, all vertices will be moved to the average Y coordinate, this is
    disabled by default. If you set move to True, you can control the
    construction history with the named parameter history, default is False
    (history is disabled).
    """

    if verts is None:
        verts = cmd.ls (sl = True, fl = True)

    if type (verts) is not list or len (verts) < 2:
        return None

    s  = 0.0
    vY = {}
    n  = 0

    for vert in verts:
        if not re.match (r"^[\w:]+\.vtx\[\d+\]$", vert):
            continue
        vY [vert] = pointCoordinates (vert)
        if vY [vert] is not None:
            vY [vert] = vY [vert] [1]
        else:
            return None
        s += vY [vert]
        n += 1

    avg = s / n

    if not move:
        return avg

    for (vert, y) in vY.items ():
        if y != avg:
            try:
                cmd.polyMoveVertex (
                    vert, ws = True, ty = avg - y, ch = history
                )
            except (RuntimeError, TypeError), error:
                return __error (error, None)

    return avg


def bridgeOppositeFaces (
        faces     = None,
        offset    = 0.001,
        tolerance = 0.000001,
        distance  = 100,
        history   = True,
    ):

    """Create a hole between opposite faces and bridge opposite edges.

    Selected faces or faces given as first parameter (one list, will be
    expanded) will be joined to one face, as will the opposite faces, then both
    new faces will be deleted and all opposing edges will be bridged, if
    possible. Each face must have an opposite face, it is retrieved using the
    oppositeFace plugin, to alter the search the named parameters offset
    (default value 0.001), tolerance (default value 1e-6) and distance (default
    value 100) may be used. See the oppositeFace plugin for more information.
    Another named parameter - history - may be used to disable the construction
    history (if possible), it defaults to True (history is enabled).

    This function will only work for reasonable geometry. The original faces
    and their opposite faces must be of the same topology and the function must
    be able to find the opposite faces. The joinFaces function must be able to
    join the original and the opposite faces. Opposite edges will be determined
    by finding the opposite vertices using only their distance (ie. for a given
    vertex from one face the vertex from the opposite face with the least
    distance to it is regarded to be the opposite one). If there is an error,
    the function may exit in the middle of the process, and you will have to
    either undo it or complete it manually.

    The return value is a tuple, the first element is either True or False,
    depending on the success of the operation. The second and third elements
    are dictionaries containing information about the deleted (joined) faces,
    the first dictionary for the first face (that one built from the original
    selection or faces specified as parameters), and second dictionary for its
    opposite face. These dictionaries may be empty in case of an error.
    """

    # Start with a few checks:
    if faces is None:
        faces = cmd.ls (sl = True, fl = True)
    elif type (faces) in [str, unicode]:
        faces = [faces]
    if not faces or type (faces) is not list:
        return __error (
                "A list of polygon faces is required.", (False, {}, {})
            )
    if filter (lambda x: not re.match (r"^\w+\.f\[\d+\]$", x), faces):
        return __error (
                "This works only for polygon faces.", (False, {}, {})
            )
    try:
        faces = cmd.filterExpand (faces, sm = 34)
    except (RuntimeError, TypeError), error:
        return __error (error, (False, {}, {}))

    # Get the name of the mesh:
    mesh = None
    for face in faces:
        temp = re.sub (r"\.f\[(\d+|\*|\d+:\d+)\]$", "", face)
        if mesh is None:
            mesh = temp
        elif mesh != temp:
            return __error (
                    "Error: Selected faces must belong to the same mesh.",
                    (False, {}, {})
                )

    # We use the faces list to store the original face info in faces [0] and
    # the opposite face's info in faces [1]...
    faces = list ([faces])
    faces.append ([])

    # For every selected (or specified) face, we search the opposite one, and
    # put it at the same index in faces [1] as the original in faces [0]. If we
    # do not find one, we return.
    for face in faces [0]:
        try:
            cmd.select (face)
            oppositeInfo = cmd.oppositeFace (
                    o = offset, t = tolerance, d = distance
                )
        except (RuntimeError, TypeError), error:
            return __error (error, (False, {}, {}))
        if oppositeInfo [0] == "-1":
            return __error (
                    "Error: No opposite face found for %s" % face,
                    (False, {}, {})
                )
        faces [1].append ("%s.f[%s]" % (mesh, oppositeInfo [0]))

    # If there's only one we can bridge the faces, nothing more to do...
    if len (faces [0]) == 1:
        return __bridgeOppositeFaces (
                faces [0] [0], faces [1] [0], history
            )

    # For more than one face, we need to join them first (both the original
    # ones and the opposite ones):
    try:
        faceSet1 = cmd.sets (faces [1], fc = True)
    except (RuntimeError, TypeError), error:
        return __error (error, (False, {}, {}))
    face1 = joinFaces (faces [0])
    if not face1:
        return __error (
                "Error: Could not join the selected faces.", (False, {}, {})
            )
    try:
        faceSet2 = cmd.sets (face1, fc = True)
        cmd.select (faceSet1)
    except (RuntimeError, TypeError), error:
        return __error (error, (False, {}, {}))
    face2 = joinFaces ()
    if not face2:
        return __error (
                "Error: Could not join the opposite faces.", (False, {}, {})
            )
    try:
        cmd.select (faceSet2)
        face1 = cmd.ls (sl = True) [0]
        cmd.delete (faceSet1, faceSet2)
    except (RuntimeError, TypeError), error:
        return __error (error, (False, {}, {}))
    if face1 and face2:
        return __bridgeOppositeFaces (face1, face2, history)
    else:
        return (False, {}, {})


def __bridgeOppositeFaces (face1, face2, history = True):

    """Bridge the two specified faces.

    Internal use only. The first two parameters must be the names of the two
    faces to bridge, an optional named parameter history (defaults to True)
    controls the construction history. The return value is a tuple as
    specified in the description for the bridgeOppositeFaces function. There is
    no error checking in here, both faces are supposed to belong to the same
    mesh.
    """

    # Face information will be saved in faceInfo, faceInfo [0] for the first
    # face, faceInfo [1] for the second.
    faceInfo = [{}, {}]
    mesh = re.sub (r"\.f\[(\d+|\*|\d+:\d+)\]$", "", face1)
    if mesh is None:
        return (False, {}, {})
    faceInfo [0] ["face"] = face1
    faceInfo [1] ["face"] = face2
    faceInfo [0] ["mesh"] = mesh
    faceInfo [1] ["mesh"] = mesh

    # Get the vertices of the faces:
    temp = []
    for i in [0, 1]:
        try:
            cmd.select (faceInfo [i] ["face"])
            info = polyInfoList (cmd.polyInfo (fv = True) [0])
        except (RuntimeError, TypeError), error:
            return __error (error, (False, {}, {}))
        faceInfo [i] ["vertOrder"] = []
        temp.append ([])
        for v in info:
            faceInfo [i] ["vertOrder"].append ("%s.vtx[%d]" % (mesh, v))
            temp [i].append ("%s.vtx[%d]" % (mesh, v))
    if len (faceInfo [0] ["vertOrder"]) != len (faceInfo [1] ["vertOrder"]):
        return __error (
                "Error: Number of vertices not equal!",
                (False, {}, {})
            )

    # And now the opposite vertices: We start with the first vertex of the
    # first face and search for the vertex of the opposite face with the
    # minimal distance. This one is then removed from the list and not
    # considered for the remaining vertices. Then we continue with the next one
    # until all are done:
    for i in [0, 1]:
        faceInfo [i] ["vertices"] = {}
        faceInfo [i] ["oppositeVertices"] = {}
    temp.append ([])
    for v1 in temp [0]:
        p1 = pointCoordinates (v1)
        if p1 is None:
            return (False, {}, {})
        faceInfo [0] ["vertices"] [v1] = tuple (p1)
        minElem = minDist = None
        for i in range (len (temp [1])):
            p2 = pointCoordinates (temp [1] [i])
            if p2 is None:
                return (False, {}, {})
            faceInfo [1] ["vertices"] [temp [1] [i]] = tuple (p2)
            dist = pointsDistance (p1, p2)
            if dist is None:
                return (False, {}, {})
            elif dist < minDist or minDist is None:
                minDist = dist
                minElem = i
        temp [2].append (temp [1].pop (minElem))
        faceInfo [0] ["oppositeVertices"] [v1] = temp [2] [-1]
        faceInfo [1] ["oppositeVertices"] [temp [2] [-1]] = v1

    # We store the edges per face:
    temp = []
    for i in [0, 1]:
        try:
            cmd.select (faceInfo [i] ["face"])
            info = polyInfoList (cmd.polyInfo (fe = True) [0])
        except (RuntimeError, TypeError), error:
            return __error (error, (False, {}, {}))
        faceInfo [i] ["edgeOrder"] = []
        temp.append ([])
        for e in info:
            faceInfo [i] ["edgeOrder"].append ("%s.e[%d]" % (mesh, e))
            temp [i].append ("%s.e[%d]" % (mesh, e))

    # And the vertices that belong to an edge. This will only work correctly if
    # every edge has only two vertices.
    faceInfo [0] ["edges"] = {}
    faceInfo [1] ["edges"] = {}
    for i in [0, 1]:
        for e in temp [i]:
            try:
                (v1, v2) = cmd.filterExpand (
                        cmd.polyListComponentConversion (
                                e, fe = True, tv = True
                            ),
                        sm = 31
                    )
            except (RuntimeError, TypeError), error:
                return __error (error, (False, {}, {}))
            faceInfo [i] ["edges"] [e] = (v1, v2)

    # That's all we need to know. Delete the two faces:
    try:
        cmd.delete (faceInfo [0] ["face"], faceInfo [1] ["face"])
    except (RuntimeError, TypeError), error:
        return _error (error, (False, {}, {}))

    # Based on the information we got, we can now find out which edges to
    # bridge, and bridge them.
    faceInfo [0] ["oppositeEdges"] = {}
    faceInfo [1] ["oppositeEdges"] = {}
    eTemp = dict (faceInfo [1] ["edges"])
    created = 0
    for e1, v1 in faceInfo [0] ["edges"].items ():
        oVerts = []
        for v in v1:
            oVerts.append (faceInfo [0] ["oppositeVertices"] [v])
        for e2, (v21, v22) in eTemp.items ():
            if v21 in oVerts and v22 in oVerts:
                try:
                    cmd.select (cl = True)
                    cmd.polyBridgeEdge (e1, e2, divisions = 0, ch = history)
                except (RuntimeError, TypeError), error:
                    return __error (error, (False, {}, {}))
                created += 1
                faceInfo [0] ["oppositeEdges"] [e1] = e2
                faceInfo [1] ["oppositeEdges"] [e2] = e1
                del eTemp [e2]
                break
        else:
            print "Warning: could not bridge edge %s" % e1

    # The newly created faces are the ones at the end of the face list, so we
    # can select them:
    try:
        noFaces = len (cmd.filterExpand ("%s.f[*]" % mesh, sm = 34))
        cmd.select ("%s.f[%d:%d]" % (mesh, noFaces - created, noFaces - 1))
    except (RuntimeError, TypeError), error:
        return __error (error, (False, {}, {}))

    # Done!
    return (True, faceInfo [0], faceInfo [1])


def cuboidFromPoints (*points):

    """Create a polygon cuboid from a list of corner vertex positions.

    The eight vertex positions defining the cube must be specified as single
    parameters, each one being an (x, y, z) tuple. The first four must be the
    vertices of one face, the last four the vertices opposite to the first
    four. They must be specified in the same order!

    Note that the normal direction depends on the order of the points. Define
    the points in clockwise order and the normals will point out of the cube.

    The function returns the name of the new surface (or None if not exactly
    eight parameters were specified or another error occured).
    """

    if len (points) != 8:
        return None

    (dn, up) = (unitCorrection (points [:4]), unitCorrection (points [4:]))

    try:
        name = cmd.polyCreateFacet (p = dn, ch = False) [0]
        cmd.polyAppendVertex (a = [1, 0, up [0], up [1]], ch = False)
        cmd.polyAppendVertex (a = [2, 1, 5,      up [2]], ch = False)
        cmd.polyAppendVertex (a = [3, 2, 6,      up [3]], ch = False)
        cmd.polyAppendVertex (a = [0, 3, 7,      4     ], ch = False)
        cmd.polyAppendVertex (a = [7, 6, 5,      4     ], ch = False)
        cmd.polyNormal (name, nm = 1, ch = False)
        cmd.xform (name, cp = True)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    return name


def dirVector (p1, p2):

    """Calculate the direction vector between two points.

    Returns a normalized direction vector from point 1 to point 2, given as
    first and second parameter. Each point must either be specified as a string
    (the vertex name) or as a list or tuple containing the world coordinates of
    the point.

    The normalized direction vector will be returned as a tuple.
    """

    p1 = pointCoordinates (p1)
    p2 = pointCoordinates (p2)
    d  = pointsDistance   (p1, p2)

    if None in [p1, p2, d] or d == 0.0:
        return None

    return tuple (map (lambda x: (x [1] - x [0]) / float (d), zip (p1, p2)))


def findVertexAt (vertices, pos, tolerance = 0.000001):

    """Find vertices at a given position.

    findVertexAt takes a list of vertices (either a list with several vertex
    specifications or a string with a single one, both may include ranges and
    wildcards using the regular Maya notation) as first parameter, and returns
    a list with those vertices found in the original list that are located
    within a radius around a position (specified as (x, y, z) tuple as second
    parameter, world space). You may also specify a list with several such
    tuples, in that case the result will be a multi dimensional list with each
    element being a list of vertices for the associated position. The radius
    may be changed with the (third) parameter named tolerance, the default
    value is 1e-6.

    The list(s) returned are expanded and may be empty, None will be returned
    if an error occurs.
    """

    if type (pos) is list:
        results = []
        for p in pos:
            results.append (findVertexAt (vertices, p))
        return results
    else:
        try:
            expanded = cmd.filterExpand (vertices, sm = 31)
        except (RuntimeError, TypeError), error:
            return __error (error, None)
        results = []
        for vert in expanded:
            dist = pointsDistance (pos, vert)
            if dist >= 0 and dist <= tolerance:
                results.append (vert)
        return results


def innerProduct (v1, v2):

    """Calculate the inner product of two vectors.

    The vectors must be specified as two single parameters and may be of
    arbitrary length. The return value is the inner product.
    """

    p = 0.0

    for (i, j) in zip (v1, v2):
        p += i * j

    return p


def isMesh (*meshes):

    """Check if the specified objects are meshes.

    Checks the objects given as parameters, or, if no parameters are specified,
    the current selection. If every object is a mesh the function will return
    True, else False. If an object itself is not a mesh, the shape node (if
    any) will be checked (eg. for transform nodes). Note that mesh components
    are not meshes, so if any components are selected False will be returned.
    """

    if not meshes:
        meshes = cmd.ls (sl = True)
        if not meshes:
            return False

    for mesh in meshes:
        if "[" in str (mesh):
            return False
        try:
            if cmd.objectType (mesh, isa = "mesh"):
                continue
        except (RuntimeError, TypeError), error:
            return __error (error)
        try:
            rel = cmd.listRelatives (mesh, s = True)
        except (RuntimeError, TypeError), error:
            return __error (error)
        if not rel or not isMesh (*rel):
            return False

    return True


def joinFaces (faces = None, history = True):

    """Delete edges between specified faces.

    The first parameter may be a list of faces, if it is not specified the
    active selection will be used. All edges shared by pairs of the specified
    faces will be deleted (the polyDelEdge command is used with cleanVertices
    set to False). An optional parameter named history may be used to turn off
    the construction history, the default value is True (history is on). Note
    that turning it off will not work if the mesh has already a history.

    Returns the name of the new face, or None if there was an error. Note that
    if not all specified faces are connected None will be returned (though all
    connected faces will be joined correctly).
    """

    if faces is None:
        faces = cmd.ls (sl = True, fl = True)

    if not faces or type (faces) is not list:
        return __error ("A list of polygon faces is required.", None)
    if filter (lambda x: not re.match (r"^\w+\.f\[\d+\]$", x), faces):
        return __error ("This works only for polygon faces.", None)

    if len (faces) == 1:
        return faces [0]

    try:
        edges = cmd.filterExpand (
                cmd.polyListComponentConversion (faces, ff = True, te = True),
                sm = 32
            )
        edgeSet = cmd.sets (edges, eg = True)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    delEdges = []

    for i in range (len (faces)):
        f1 = faces [i]
        for f2 in faces [i+1:]:
            try:
                cmd.select (f1, f2)
            except (RuntimeError, TypeError), error:
                return __error (error, None)
            shared = sharedComponents (fe = True)
            if shared:
                delEdges.extend (shared)

    if delEdges:
        try:
            cmd.select (delEdges)
            cmd.polyDelEdge (ch = history)
        except (RuntimeError, TypeError), error:
            return __error (error, None)
    else:
        return None

    try:
        cmd.select (edgeSet)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    face = sharedComponents (ef = True)

    try:
        cmd.delete (edgeSet)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    if len (face) != 1:
        return None

    return face [0]


def parentNode (child = None):

    """Get the first parent of an object.

    The object to query must be specified as first parameter, if it is None
    (default), the current selection will be used, but only if only one object
    is selected. The return value is the name of the parent if one could be
    found, None in any other case.

    This function may also be used to get the mesh shape name for single
    components.
    """

    if child is None:
        try:
            child = cmd.ls (sl = True)
        except (RuntimeError, TypeError), error:
            return __error (error, None)
        if len (child) != 1:
            return __error ("Only one object must be selected", None)
        child = child [0]

    try:
        parent = cmd.listRelatives (child, p = True)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    if parent:
        parent = parent [0]

    return parent


def parentTransform (child = None):

    """Get the first transform node parent of an object.

    One parameter specifying the object to get the parent for, if it is not
    specified the current selection will be used.
    """

    if child is None:
        try:
            child = cmd.ls (sl = True)
        except (RuntimeError, TypeError), error:
            return __error (error, None)
        if len (child) != 1:
            return __error ("Only one object must be selected", None)
        child = child [0]

    try:
        while cmd.objectType (child) != "transform":
            child = parentNode (child)
    except (RuntimeError, TypeError), error:
        return __error (error)

    return child


def pointBetween (p1, p2):

    """Get the center of the line segment between two points.

    The two points must be specified as single parameters, either as vertex
    names or (x, y, z)-tuples. The return value will be a tuple specifying the
    world coordinates of the center, or None in case of an error.
    """

    p1 = pointCoordinates (p1)
    p2 = pointCoordinates (p2)

    if None in [p1, p2]:
        return None

    p = []
    for (x, y) in zip (p1, p2):
        p.append ((x + y) / 2.0)

    return tuple (p)


def pointCoordinates (point):

    """Get a coordinate tuple for a specified vertex.

    This function takes one parameter: either a vertex name, in that case the
    return value will be a tuple containing the (x, y, z) coordinates of this
    vertex (world space), or a tuple or list, in that case the function does
    nothing and returns the parameter itself. If an error occurs, None will be
    returned.
    """

    if type (point) in [str, unicode]:
        try:
            point = cmd.pointPosition (point)
        except (RuntimeError, TypeError), error:
            return __error (error, None)
    elif not type (point) in [list, tuple]:
        return __error ("Invalid point: %s" % point, None)

    return tuple (point)


def pointInDir (start, direction, distance, scale = 1.0):

    """Find a point in a specified direction.

    The parameters are the starting point ((x, y, z) tuple or list, or a name
    of a vertex to get the position from), the direction vector (usually a
    tuple or list, it must be already normalized in this case, if you specify a
    string it is interpreted as a vertex name and the direction vector is
    calculated as normalized vector in the direction from start to to this
    vertex) and the distance (usually a numeric value, if you specify this as
    string, the distance will be set to the distance between the start point
    and the vertex represented by that string). An optional fourth parameter
    (named scale) may be used for scaling, it defaults to 1.0. To get the point
    in the opposite direction use a scale factor of -1.0.

    Returns the point as (x, y, z) tuple, or None in case of an error.
    """

    start = pointCoordinates (start)

    if type (direction) in [str, unicode]:
        direction = dirVector (start, direction)

    if type (distance) in [str, unicode]:
        distance = pointsDistance (start, distance)

    if None in [start, direction, distance]:
        return None

    return tuple (
            map (
                    lambda x: x [0] + x [1] * distance * scale,
                    zip (start, direction)
                )
        )


#      v1      Given three points (or vertices) v1, v2 and v3 and a distance d,
#     /        this function will return a point p on the straight line between
#    v2---v3   v1 and v2. This point will be located in the distance d from the
#   / |        line between v2 and v3. See the draft on the left for details...
#  /  |d       The point will be returned as a (x, y, z)-tuple, in world space.
# p---+        Neat, now all the lines here above have the exact same length :)

def pointInDist (v1, v2, v3, d):

    """Get a point in a specified distance from a line.

    Given three points (or vertex names) v1, v2 and v3 (single parameters) and
    a distance d, this function will return a point p on the straight line
    through v1 and v2. This point will be located in the distance d from the
    line between v2 and v3. Please see the source code for a simple draft.

    The point will be returned as a (x, y, z)-tuple, in world space. If an
    error occurs, None will be returned.
    """

    d1 = dirVector (v2, v1)
    d2 = dirVector (v2, v3)

    if None in [d1, d2]:
        return None

    dist = d / math.sqrt (1 - innerProduct (d1, d2) ** 2)

    return pointInDir (v2, d1, dist, -1)


def pointsDistance (p1, p2):

    """Calculate the euclidean distance between two points.

    The two points must be given as two parameters, either lists or tuples
    specifying the coordinates, or as two strings containing vertex names. Note
    that if you specify the points as lists, these lists may be of arbitrary
    length.

    Returns None if the lists are of different length or in case of another
    error, else the distance.
    """

    p1 = pointCoordinates (p1)
    p2 = pointCoordinates (p2)

    if None in [p1, p2] or len (p1) != len (p2):
        return None

    sum = 0.0
    for elem in zip (p1, p2):
        sum += (elem [0] - elem [1]) ** 2

    return math.sqrt (sum)


def polyArea2D (points, ignoreX = False, ignoreZ = False):

    """Calculate the signed area of a 2D polygon.

    The first and only mandatory parameter is a list of points (either vertex
    names or (x, y, z)-tuples). The function will calculate the signed area of
    the polygon defined by these points. In the default configuration, the Y
    coordinate will be ignored. To ignore the X coordinate, set the named
    parameter ignoreX to True, or ignoreZ for the Z coordinate to be ignored.

    This will only work for planar polygons parallel to the XZ plane (or the
    according plane if you ignore another coordinate).

    If there is an error, None will be returned, else the area of the polygon.
    """

    points = map (lambda x: pointCoordinates (x), points)

    if None in points:
        return None

    points.extend (points [:2])

    a = 0
    b = 2

    if ignoreX and not ignoreZ:
        a = 1
    elif ignoreZ and not ignoreX:
        b = 1

    area = 0.0

    i = 1
    while i < len (points) - 1:
        area += points [i] [a] * (points [i + 1] [b] - points [i - 1] [b])
        i += 1

    return area / 2.0


def polyArea3D (points):

    """Calculate the (unsigned) area of a 3D polygon.

    Takes one parameter, a list of points (vertex names or (x, y, z)-tuples)
    that specifies the polygon. Returns the (unsigned) area of that polygon,
    or - in case of an error - None. Note that this will only work for planar
    polygons!
    """

    x = polyArea2D (points, ignoreX = True)
    y = polyArea2D (points)
    z = polyArea2D (points, ignoreZ = True)

    if None in (x, y, z):
        return None

    return math.sqrt (x ** 2 + y ** 2 + z ** 2)


def polyInfoList (info, onlyInt = True, onlyFloat = False):

    """Split return strings of Maya's polyInfo command.

    Maya's polyInfo command returns a list of strings containing the requested
    information. Use polyInfoList to split such strings and return a list of
    the stuff.

    The first parameter must be the string to split (only one at a time).

    Note that the return list will not contain full component names, the type
    of information returned depends on the requested polyInfo information. You
    may filter the information using the onlyInt (defaults to True) or the
    onlyFloat (defaults to False) flags, if one is set to True only elements
    that can successfully be converted to the appropriate type will be
    returned, all other elements will be discarded.
    """

    list = re.sub (r"^[a-zA-Z_0-9 \-]+: +", "", info.rstrip ()).split ()

    if onlyFloat:
        floatList = []
        for value in list:
            try:
                floatList.append (float (value))
            except ValueError:
                print "Warning: Can not convert %s to float." % value
        return floatList
    elif onlyInt:
        intList = []
        for value in list:
            try:
                intList.append (int (value))
            except ValueError:
                print "Warning: Can not convert %s to int." % value
        return intList

    return list


def reduceComponents (components, sm):

    """Remove duplicate components from a list.

    The first parameter is a list of components, the second is the component
    type ID as used in Maya's filterExpand function. The list will be processed
    by filterExpand, converting shape components to transform node ones (eg.
    polySurfaceShape1.f[0] will be converted to polySurface1.f[0]) and then all
    duplicate components will be removed. The return value is the resulting
    list, or None if there was an error.
    """

    try:
        components = cmd.filterExpand (components, sm = sm)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    components = set (components)

    return list (components)


def removeDuplicateFaces (*meshes):

    """Remove duplicate faces in a mesh.

    If there are duplicate faces in a mesh (ie. if more than one face is
    defined by the exact same vertices), this function will delete all of these
    faces. An arbitrary number of meshes may be specified as single parameters,
    if none are given, the active selection will be used.

    Returns the total number of faces deleted, or None if there were no meshes
    specified and none selected either or in case of another error.

    Note: The function will try to disable the construction history for the
    delete operation, this will only work if the mesh does not yet have a
    history. This behaviour can not be changed.
    """

    if not meshes:
        meshes = cmd.ls (sl = True)
        if not meshes:
            return None

    if not isMesh (*meshes):
        return __error ("removeDuplicateFaces works for meshes only.", None)

    deleted = 0
    for mesh in set (meshes):
        deleted += __removeDuplicateFaces (mesh)

    return deleted


def __removeDuplicateFaces (mesh):

    """Remove duplicate faces in a mesh.

    Internal use only. Takes one parameter: the name of the mesh to clean up.
    Returns the number of faces deleted. Returns 0 in case of an error.
    """

    try:
        faces = cmd.filterExpand ("%s.f[*]" % mesh, sm = 34)
    except (RuntimeError, TypeError), error:
        return __error (error, 0)

    if len (faces) < 2:
        return 0

    deleteFaces = set ()

    for face in faces:
        if face in deleteFaces:
            continue
        try:
            verts = cmd.filterExpand (
                    cmd.polyListComponentConversion (
                            face, ff = True, tv = True
                        ),
                    sm = 31
                )
        except (RuntimeError, TypeError), error:
            return __error (error, 0)
        sharedFaces = sharedComponents (verts, vf = True)
        if len (sharedFaces) == 1:
            continue
        for sharedFace in sharedFaces:
            try:
                sharedVerts = cmd.filterExpand (
                        cmd.polyListComponentConversion (
                                sharedFace, ff = True, tv = True
                            ),
                        sm = 31
                    )
            except (RuntimeError, TypeError), error:
                return __error (error, 0)
            if len (sharedVerts) == len (verts):
                deleteFaces.add (sharedFace)
                deleteFaces.add (face)

    if len (deleteFaces) > 0:
        try:
            cmd.polyDelFacet (list (deleteFaces), ch = False)
        except (RuntimeError, TypeError), error:
            return __error (error, 0)

    return len (deleteFaces)


def sharedComponents (components = None, **flags):

    """Get vertices/edges/faces shared by some vertices/edges/faces.

    sharedComponents basically works like Maya's polyInfo command, except that
    it returns only those components that are common to all input components.

    You may specify the input components in a list as first parameter, but
    sharedComponents needs to select them for polyInfo to work, so your active
    selection will be lost. Instead of using a parameter you may also select
    the components by yourself.

    The returning list contains the full names of the shared components (not
    only the indices like Maya's polyInfo command), and may be empty. Note that
    there are no restrictions to the flags you may use, in general all flags
    supported by polyInfo are allowed. Not all of these do make sense, though,
    and you must not specify more than one flag at a time (this is not checked,
    but you won't get meaningful results if you do it).
    """

    if components:
        try:
            cmd.select (components)
        except (RuntimeError, TypeError), error:
            return __error (error, [])

    try:
        sel = cmd.ls (sl = True)
    except (RuntimeError, TypeError), error:
        return __error (error, [])

    if len (sel) < 1:
        return []

    mesh = None

    for comp in sel:
        temp = re.sub (r"\.(f|e|vtx)\[(\d+|\*|\d+:\d+)\]$", "", comp)
        if temp is None:
            return []
        if mesh is None:
            mesh = temp
        elif mesh != temp:
            return []

    try:
        infoList = cmd.polyInfo (**flags)
    except (RuntimeError, TypeError), error:
        return __error (error, [])

    if not infoList:
        return []

    target = ""
    for flag, value in flags.items ():
        if flag not in ["fv", "fe", "ev", "ef", "ve", "vf"]:
            print "Warning: Flag %s is probably useless or invalid." % flag
            continue
        if not value:
            continue
        target = flag [1]
        break
    else:
        return []

    if target == "v":
        target = "vtx"

    shared = None
    for infoStr in infoList:
        indexList = polyInfoList (infoStr)
        componentList = []
        for index in indexList:
            componentList.append ("%s.%s[%d]" % (mesh, target, index))
        componentList = set (componentList)
        if shared is None:
            shared = componentList
        else:
            shared = shared.intersection (componentList)
            if len (shared) == 0:
                return []

    return list (shared)


def unitCorrection (stuff):

    """Convert values from current working unit to cm.

    Thousands of Euros for Maya and they couldn't even manage to use the same
    unit for all commands...

    Only one parameter, either a single value or a list/tuple of values, all
    integer or float (other types will be ignored). A multidimensional list
    will work, too. Returns the converted value or list, or - if an error
    occurs - None (in case of lists single elements in the resulting list may
    be None).
    """

    try:
        currentUnit = cmd.currentUnit (q = True, l = True)
    except (RuntimeError, TypeError), error:
        return __error (error, None)

    if currentUnit == "cm":
            return stuff

    try:
        factor = float (
                re.sub (
                        r"[a-zA-Z]*$",
                        "",
                        # Seems to work with "1" as a string in 2009,
                        # definitely does not work with 1 as a number in 8.5.
                        cmd.convertUnit ("1", f = currentUnit, t = "cm")
                    )
            )
    except ValueError:
        return __error ("Could not convert factor to float.", None)

    if type (stuff) in [int, long, float]:
        return factor * stuff

    conv = []
    if type (stuff) in [list, tuple]:
        for elem in stuff:
            conv.append (unitCorrection (elem))

    return type (stuff) (conv)


def vertsAdjustVertical (verts):

    """Move a list of vertices directly above (or below) a reference point.

    The function requires one parameter, a list of vertex names. The first
    element of this list will be the reference element (and this element may be
    specified as (x, y, z)-tuple), all remaining vertices in the list will be
    moved to the same X and Z coordinates, while keeping their original Y
    coordinate. Returns False in case of an error, else True.
    """

    if type (verts) is not list or len (verts) < 2:
        return False

    p1 = pointCoordinates (verts [0])
    if p1 is None:
        return False

    for v in verts [1:]:
        p2 = pointCoordinates (v)
        if p2 is None:
            return False
        if p2 [0] != p1 [0] or p2 [2] != p1 [2]:
            try:
                cmd.polyMoveVertex (
                        v,
                        ws = True,
                        tx = p1 [0] - p2 [0],
                        tz = p1 [2] - p2 [2]
                    )
            except (RuntimeError, TypeError), error:
                return __error (error)

    return True


def __error (msg = "Unknown error.", ret = False):

    """Print an error message and return False.

    Internal use only. The parameter msg specifies the error message, it
    defaults to "Unknown error". Return value can be set with the ret
    parameter, it defaults to False.
    """

    print "Error: %s" % msg
    return ret


print "polyFunctions.py version %s imported" % __version


# vim: set et si nofoldenable ft=python sts=4 sw=4 tw=79 ts=4 fenc=utf8 :
# :indentSize=4:tabSize=4:noTabs=true:mode=python:maxLineLen=79: